import com.example.product.service.ProductService;
import com.example.product.service.impl.ProductServiceImpl;
import com.example.subscription.service.SubscriptionService;
import com.example.subscription.service.impl.SubscriptionServiceImpl;
import com.example.user.service.UserService;
import com.example.user.service.impl.UserServiceImpl;

public class Application {
    public static void main(String[] args) {
        UserService userService = UserServiceImpl.getInstance();
        SubscriptionService subscriptionService = SubscriptionServiceImpl.getInstance();
        ProductService productService = ProductServiceImpl.getInstance();
        ((ProductServiceImpl) productService).setSubscriptionService(subscriptionService);
        ((SubscriptionServiceImpl) subscriptionService).setUserService(userService);
        ((SubscriptionServiceImpl) subscriptionService).setProductService(productService);
    }
}
