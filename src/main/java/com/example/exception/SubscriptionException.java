package com.example.exception;

public class SubscriptionException extends RuntimeException {
    public SubscriptionException() {}

    public SubscriptionException(String message) {
        super(message);
    }
}
