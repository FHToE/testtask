package com.example.notification;

import com.example.user.model.User;

import java.util.List;

public class DummyNotifier {
    public void notifyUsers(List<User> users, String productId) {
        users.forEach(u -> System.out.printf("User %s notified by product %s%n", u.getName(), productId));
    }
}
