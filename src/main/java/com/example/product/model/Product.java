package com.example.product.model;

import com.example.exception.ProductException;

public class Product {

    private final String id;
    private final ProductCategory category;
    private Boolean inStock;

    public Product(String id, ProductCategory category, Boolean isInStock) {
        validate(id, category, isInStock);
        this.id = id;
        this.category = category;
        this.inStock = isInStock;
    }

    public boolean isInStock() {
        return inStock;
    }

    public void setInStock(boolean inStock) {
        this.inStock = inStock;
    }

    public String getId() {
        return id;
    }

    public ProductCategory getCategory() {
        return category;
    }

    private void validate(String id, ProductCategory category, Boolean isInStock) {
        if (id == null || id.trim().isEmpty() || category == null || isInStock == null) {
            throw new ProductException("Invalid Product parameter");
        }
    }
}
