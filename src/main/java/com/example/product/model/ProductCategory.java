package com.example.product.model;

public enum ProductCategory {

    MEDICAL, BOOKS, DIGITAL

}
