package com.example.product.service;

import com.example.exception.ProductException;
import com.example.product.model.Product;
import com.example.product.model.ProductCategory;

import java.util.List;

public interface ProductService {
    Product addNewProduct(String id, ProductCategory category, Boolean inStock);

    void deleteAllProducts();

    Product deleteProductById(String id);

    List<Product> getProducts();

    default Product getProductById(String id) {
        throw new ProductException();
    }

    void addToStockById(String id);

    void removeFromStockById(String id);

    default Boolean isProductExists(String id) {
        try {
            getProductById(id);
        } catch (ProductException e) {
            return false;
        }
        return true;
    }
}
