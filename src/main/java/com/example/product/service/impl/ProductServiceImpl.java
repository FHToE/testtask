package com.example.product.service.impl;

import com.example.exception.ProductException;
import com.example.product.model.Product;
import com.example.product.model.ProductCategory;
import com.example.product.service.ProductService;
import com.example.subscription.service.SubscriptionService;

import java.util.ArrayList;
import java.util.List;

public class ProductServiceImpl implements ProductService {
    private final List<Product> productList;
    private SubscriptionService subscriptionService;
    private static final ProductService productService = new ProductServiceImpl();

    public static ProductService getInstance() {
        return productService;
    }

    private ProductServiceImpl() {
        this.productList = new ArrayList<>();
    }

    public void setSubscriptionService(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @Override
    public Product addNewProduct(String id, ProductCategory category, Boolean inStock) {
        if (isProductExists(id)) {
            throw new ProductException(String.format("Product %s already exists", id));
        }
        Product product = new Product(id, category, inStock);
        productList.add(product);
        return product;
    }

    @Override
    public void deleteAllProducts() {
        productList.clear();
    }

    @Override
    public Product deleteProductById(String id) {
        if (!isProductExists(id)) {
            throw new ProductException(String.format("Product %s doesn't exist", id));
        }
        Product product = getProductById(id);
        productList.remove(product);
        return product;
    }

    @Override
    public List<Product> getProducts() {
        return List.copyOf(productList);
    }

    @Override
    public Product getProductById(String id) {
        return productList.stream()
                .filter(p -> p.getId().equals(id))
                .findAny()
                .orElseThrow(() -> new ProductException(String.format("Product %s doesn't exist", id)));
    }

    @Override
    public void addToStockById(String id) {
        if (!isProductExists(id)) {
            throw new ProductException(String.format("Product %s doesn't exist", id));
        }
        Product product = getProductById(id);
        product.setInStock(true);
        subscriptionService.notifyUsers(id);
    }

    @Override
    public void removeFromStockById(String id) {
        if (!isProductExists(id)) {
            throw new ProductException(String.format("Product %s doesn't exist", id));
        }
        Product product = getProductById(id);
        product.setInStock(false);
    }
}
