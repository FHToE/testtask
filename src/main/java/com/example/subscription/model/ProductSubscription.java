package com.example.subscription.model;

import com.example.exception.SubscriptionException;
import com.example.product.model.Product;
import com.example.user.model.User;

import java.util.ArrayList;
import java.util.List;

public class ProductSubscription {

    private final Product product;
    private final List<User> users;

    public Product getProduct() {
        return product;
    }

    public List<User> getUsers() {
        return List.copyOf(users);
    }

    public void deleteAllUsers() {
        users.clear();
    }

    public void addUser(User user) {
        if (users.contains(user)) {
            throw new SubscriptionException(String.format("User %s is already subscribed to the product %s", user.getName(), product.getId()));
        }
        users.add(user);
    }

    public void deleteUser(User user) {
        if (!users.contains(user)) {
            throw new SubscriptionException(String.format("User %s is not subscribed to the product %s", user.getName(), product.getId()));
        }
        users.remove(user);
    }

    private ProductSubscription(Product product, List<User> users) {
        validate(product);
        this.product = product;
        this.users = users;
    }

    private void validate(Product product) {
        if (product == null) {
            throw new SubscriptionException("Invalid subscription parameter");
        }
    }

    public ProductSubscription(Product product) {
        this(product, new ArrayList<>());
    }
}
