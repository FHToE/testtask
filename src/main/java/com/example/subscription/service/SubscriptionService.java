package com.example.subscription.service;

import com.example.product.model.ProductCategory;
import com.example.subscription.model.ProductSubscription;
import com.example.user.model.User;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public interface SubscriptionService {

    void subscribeUser(String userName, String productId);

    void deleteAllSubscriptions();

    void unSubscribe(String userName, String productId);

    void notifyUsers(String productId);

    ProductSubscription getSubscriptionByProduct(String productId);

    List<ProductSubscription> getSubscriptions();

    default List<User> sortUsersByPriority(List<User> users, ProductCategory productCategory) {
        List<User> modifiableList = new ArrayList<>(users);
        Comparator<User> comparator = Comparator.comparing(user -> getUserPriority(user, productCategory));
        modifiableList.sort(comparator);
        return modifiableList;
    }

    int getUserPriority(User user, ProductCategory category);
}
