package com.example.subscription.service.impl;

import com.example.notification.DummyNotifier;
import com.example.product.service.ProductService;
import com.example.subscription.model.ProductSubscription;
import com.example.subscription.service.SubscriptionService;
import com.example.exception.SubscriptionException;
import com.example.product.model.Product;
import com.example.product.model.ProductCategory;
import com.example.user.model.User;
import com.example.user.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionServiceImpl implements SubscriptionService {
    private final List<ProductSubscription> subscriptions;
    private DummyNotifier notifier;
    private UserService userService;
    private ProductService productService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    private static final SubscriptionService subscriptionService = new SubscriptionServiceImpl();

    private SubscriptionServiceImpl() {
        this.subscriptions = new ArrayList<>();
        this.notifier = new DummyNotifier();
    }

    public static SubscriptionService getInstance() {
        return subscriptionService;
    }

    public void setNotifier(DummyNotifier notifier) {
        this.notifier = notifier;
    }

    @Override
    public void subscribeUser(String userName, String productId) {
        ProductSubscription subscription = getSubscriptionByProduct(productId);
        User user = userService.getUserByName(userName);
        if (subscription == null) {
            subscription = new ProductSubscription(productService.getProductById(productId));
            subscriptions.add(subscription);
        }
        subscription.addUser(user);
    }

    @Override
    public void deleteAllSubscriptions() {
        subscriptions.clear();
    }

    @Override
    public void unSubscribe(String userName, String productId) {
        ProductSubscription subscription = getSubscriptionByProduct(productId);
        User user = userService.getUserByName(userName);
        if (subscription == null) {
            throw new SubscriptionException(String.format("User %s is not subscribed to the product %s",
                    user.getName(), productId));
        }
        subscription.deleteUser(user);
    }

    @Override
    public void notifyUsers(String productId) {
        ProductSubscription productSubscription = getSubscriptionByProduct(productId);
        if (productSubscription == null) {
            return;
        }
        List<User> sortedUsers = sortUsersByPriority(productSubscription.getUsers(), productSubscription.getProduct().getCategory());
        notifier.notifyUsers(sortedUsers, productSubscription.getProduct().getId());
    }

    @Override
    public ProductSubscription getSubscriptionByProduct(String productId) {
        Product product = productService.getProductById(productId);
        return subscriptions.stream().filter(s -> s.getProduct().equals(product)).findAny().orElse(null);
    }

    @Override
    public List<ProductSubscription> getSubscriptions() {
        return List.copyOf(subscriptions);
    }

    @Override
    public int getUserPriority(User user, ProductCategory category) {
        if (user.isPremium() || (category == ProductCategory.MEDICAL && user.getAge() > 70)) {
            return 0;
        }
        return 1;
    }
}
