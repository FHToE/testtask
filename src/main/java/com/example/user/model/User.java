package com.example.user.model;

import com.example.exception.UserException;

public class User {

    private String name;
    private Boolean premium;
    private Integer age;

    public User(String name, Boolean premium, int age) {
        validate(name, age, premium);
        this.name = name;
        this.premium = premium;
        this.age = age;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isPremium() {
        return premium;
    }

    public void setPremium(Boolean premium) {
        this.premium = premium;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    private void validate(String name, Integer age, Boolean premium) {
        if (age == null || name == null || name.trim().isEmpty() || age < 0 || age > 150 || premium == null) {
            throw new UserException("Invalid User parameter");
        }
    }
}
