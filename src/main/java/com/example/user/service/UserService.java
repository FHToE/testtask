package com.example.user.service;

import com.example.exception.UserException;
import com.example.user.model.User;

import java.util.List;

public interface UserService {
    User addUser(String name, Boolean isPremium, Integer age);

    default User getUserByName(String name) {
        throw new UserException();
    }

    void deleteAllUsers();

    User deleteUserByName(String name);

    List<User> getUsers();

    default Boolean isUserExists(String name) {
        try {
            getUserByName(name);
        } catch (UserException e) {
            return false;
        }
        return true;
    }
}
