package com.example.user.service.impl;

import com.example.exception.UserException;
import com.example.user.model.User;
import com.example.user.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {
    private final List<User> userList;
    private static final UserService userService = new UserServiceImpl();

    public static UserService getInstance() {
        return userService;
    }

    private UserServiceImpl() {
        this.userList = new ArrayList<>();
    }

    @Override
    public User addUser(String name, Boolean isPremium, Integer age) {
        if (isUserExists(name)) {
            throw new UserException(String.format("User with name %s already exists", name));
        }
        User user = new User(name, isPremium, age);
        userList.add(user);
        return user;
    }

    @Override
    public User getUserByName(String name) {
        return userList.stream()
                .filter(u -> u.getName().equals(name))
                .findAny()
                .orElseThrow(() -> new UserException(String.format("User %s not found", name)));
    }

    @Override
    public void deleteAllUsers() {
        userList.clear();
    }

    @Override
    public User deleteUserByName(String name) {
        if (!isUserExists(name)) {
            throw new UserException(String.format("User with name %s doesn't exist", name));
        }
        User user = getUserByName(name);
        userList.remove(user);
        return user;
    }

    @Override
    public List<User> getUsers() {
        return List.copyOf(userList);
    }
}
