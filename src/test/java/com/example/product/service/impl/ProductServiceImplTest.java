package com.example.product.service.impl;

import com.example.exception.ProductException;
import com.example.notification.DummyNotifier;
import com.example.product.model.Product;
import com.example.product.model.ProductCategory;
import com.example.product.service.ProductService;
import com.example.subscription.service.SubscriptionService;
import com.example.subscription.service.impl.SubscriptionServiceImpl;
import com.example.user.service.UserService;
import com.example.user.service.impl.UserServiceImpl;
import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ProductServiceImplTest {
    DummyNotifier spyDummyNotifier;
    SubscriptionService subscriptionService;
    ProductService productService;
    UserService userService;

    @BeforeAll
    void init() {
        subscriptionService = SubscriptionServiceImpl.getInstance();
        productService = ProductServiceImpl.getInstance();
        userService = UserServiceImpl.getInstance();
        ((ProductServiceImpl) productService).setSubscriptionService(subscriptionService);
        ((SubscriptionServiceImpl) subscriptionService).setProductService(productService);
        ((SubscriptionServiceImpl) subscriptionService).setUserService(userService);
    }

    @BeforeEach
    void seedService() {
        spyDummyNotifier = spy(DummyNotifier.class);
        ((SubscriptionServiceImpl) subscriptionService).setNotifier(spyDummyNotifier);
        productService.addNewProduct("product 1", ProductCategory.BOOKS, false);
        productService.addNewProduct("product 2", ProductCategory.MEDICAL, true);
        productService.addNewProduct("product 3", ProductCategory.DIGITAL, true);
        userService.addUser("user-test", false, 45);
        subscriptionService.subscribeUser("user-test", "product 1");
    }

    @AfterEach
    void cleanUp() {
        productService.deleteAllProducts();
        userService.deleteAllUsers();
        subscriptionService.deleteAllSubscriptions();
    }

    @Test
    void whenAddNewProduct_thenProductAdded() {
        Product product = productService.addNewProduct("test name", ProductCategory.BOOKS, true);
        assertTrue(productService.getProducts().contains(product));
        assertEquals(4, productService.getProducts().size());
    }

    @Test
    void whenAddExistingProduct_thenExceptionThrown() {
        assertThrows(ProductException.class, () -> productService.addNewProduct("product 1", ProductCategory.MEDICAL, true));
        assertThrows(ProductException.class, () -> productService.addNewProduct("product 1", ProductCategory.BOOKS, true));
        assertEquals(3, productService.getProducts().size());
    }

    @Test
    void whenDeleteExistingProduct_thenProductDeleted() {
        Product product = productService.deleteProductById("product 1");
        assertFalse(productService.getProducts().contains(product));
        assertEquals(2, productService.getProducts().size());
    }

    @Test
    void whenDeleteNonExistingProduct_thenExceptionThrown() {
        assertThrows(ProductException.class, () -> productService.deleteProductById("product 10"));
        assertEquals(3, productService.getProducts().size());
    }

    @Test
    void whenGetProducts_thenReturnProducts() {
        List<Product> products = productService.getProducts();
        assertEquals(3, products.size());
    }

    @Test
    void whenGetProductByValidId_ThenReturnProduct() {
        Product product = productService.getProductById("product 1");
        assertNotNull(product);
    }

    @Test
    void whenGetProductByNonExistentId_thenNullReturned() {
        assertThrows(ProductException.class, () -> productService.getProductById("non-existent id"));
    }

    @Test
    void whenDeleteAllProducts_thenProductsDeleted() {
        productService.deleteAllProducts();
        assertEquals(0, productService.getProducts().size());
    }

    @Test
    void whenModifyProductListDirectly_thenActualProductListNotChangedAndExceptionThrown() {
        assertThrows(UnsupportedOperationException.class, () -> productService.getProducts().remove(1));
        assertEquals(3, productService.getProducts().size());

        assertThrows(UnsupportedOperationException.class, () -> productService.getProducts().clear());
        assertEquals(3, productService.getProducts().size());
    }

    @Test
    void whenAddProductToStockByExistingId_thenProductAddedInStock() {
        String id = "product 1";
        Product product = productService.getProductById(id);
        productService.addToStockById(id);
        assertTrue(product.isInStock());
    }

    @Test
    void whenAddProductToStockByExistingId_thenCustomersNotified() {
        String productId = "product 1";
        productService.getProductById(productId);
        userService.addUser("user1", false, 42);
        ((SubscriptionServiceImpl) subscriptionService).setNotifier(spyDummyNotifier);
        subscriptionService.subscribeUser("user1", productId);
        productService.addToStockById(productId);
        verify(spyDummyNotifier, times(1)).notifyUsers(anyList(), anyString());
    }

    @Test
    void whenAddProductToStockByNonExistingId_thenExceptionThrown() {
        String id = "product 10";
        assertThrows(ProductException.class, () -> productService.addToStockById(id));
        assertThrows(ProductException.class, () -> productService.getProductById(id));
    }

    @Test
    void whenGetProducts_thenProductsReturned() {
        assertEquals(3, productService.getProducts().size());
    }

    @Test
    void whenRemoveFromStockByIdByExistingId_thenProductRemovedFromStock() {
        String id = "product 1";
        Product product = productService.getProductById(id);
        ((SubscriptionServiceImpl) subscriptionService).setNotifier(spyDummyNotifier);
        productService.removeFromStockById(id);
        verify(spyDummyNotifier, times(0)).notifyUsers(anyList(), anyString());
        assertFalse(product.isInStock());
    }

    @Test
    void whenRemoveFromStockByIdByNonExistingId_thenExceptionThrown() {
        String id = "product 10";
        assertThrows(ProductException.class, () -> productService.removeFromStockById(id));
        assertThrows(ProductException.class, () -> productService.getProductById(id));
    }
}