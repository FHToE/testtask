package com.example.subscription.service.impl;

import com.example.exception.SubscriptionException;
import com.example.notification.DummyNotifier;
import com.example.product.model.ProductCategory;
import com.example.product.service.ProductService;
import com.example.product.service.impl.ProductServiceImpl;
import com.example.subscription.model.ProductSubscription;
import com.example.subscription.service.SubscriptionService;
import com.example.user.model.User;
import com.example.user.service.UserService;
import com.example.user.service.impl.UserServiceImpl;
import org.junit.jupiter.api.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SubscriptionServiceImplTest {
    SubscriptionService subscriptionService;
    ProductService productService;
    UserService userService;
    DummyNotifier spyDummyNotifier;

    @BeforeAll
    void init() {
        subscriptionService = SubscriptionServiceImpl.getInstance();
        productService = ProductServiceImpl.getInstance();
        userService = UserServiceImpl.getInstance();
        ((ProductServiceImpl) productService).setSubscriptionService(subscriptionService);
        ((SubscriptionServiceImpl) subscriptionService).setProductService(productService);
        ((SubscriptionServiceImpl) subscriptionService).setUserService(userService);
    }

    @BeforeEach
    void initService() {
        spyDummyNotifier = spy(DummyNotifier.class);
        ((SubscriptionServiceImpl) subscriptionService).setNotifier(spyDummyNotifier);

        String userName1 = "user1";
        String userName2 = "user2";
        String userName3 = "user3";
        String userName4 = "user4";
        String userName5 = "user5";
        String userName6 = "user6";
        String productId1 = "product1";
        String productId2 = "product2";

        userService.addUser(userName1, false, 42);
        userService.addUser(userName2, true, 32);
        userService.addUser(userName3, false, 35);
        userService.addUser(userName4, false, 75);
        userService.addUser(userName5, false, 70);
        userService.addUser(userName6, true, 75);
        productService.addNewProduct(productId1, ProductCategory.MEDICAL, true);
        productService.addNewProduct(productId2, ProductCategory.DIGITAL, true);

        //subscribe to medical category, sorted by priority should be 2 4 6 1 3 5
        subscriptionService.subscribeUser(userName1, productId1);
        subscriptionService.subscribeUser(userName2, productId1);
        subscriptionService.subscribeUser(userName3, productId1);
        subscriptionService.subscribeUser(userName4, productId1);
        subscriptionService.subscribeUser(userName5, productId1);
        subscriptionService.subscribeUser(userName6, productId1);

        //subscribe to non-medical category, sorted by priority should be 2 6 1 3 4 5
        subscriptionService.subscribeUser(userName1, productId2);
        subscriptionService.subscribeUser(userName2, productId2);
        subscriptionService.subscribeUser(userName3, productId2);
        subscriptionService.subscribeUser(userName4, productId2);
        subscriptionService.subscribeUser(userName5, productId2);
        subscriptionService.subscribeUser(userName6, productId2);
    }

    @AfterEach
    void cleanUp() {
        subscriptionService.deleteAllSubscriptions();
        userService.deleteAllUsers();
        productService.deleteAllProducts();
    }

    @Test
    void whenSubscribe_thenSubscribed() {
        String userName = "user";
        String productId = "product";
        User user = userService.addUser(userName, false, 42);
        productService.addNewProduct(productId, ProductCategory.MEDICAL, true);
        subscriptionService.subscribeUser(userName, productId);
        assertTrue(subscriptionService.getSubscriptionByProduct(productId).getUsers().contains(user));
        assertEquals(1, subscriptionService.getSubscriptionByProduct(productId).getUsers().size());
        assertEquals(3, subscriptionService.getSubscriptions().size());
    }

    @Test
    void whenSubscribeAlreadySubscribedUser_thenExceptionThrown() {
        String userName = "user1";
        String productId = "product1";
        assertThrows(SubscriptionException.class, () -> subscriptionService.subscribeUser(userName, productId));
        assertEquals(6, subscriptionService.getSubscriptionByProduct(productId).getUsers().size());
    }

    @Test
    void whenUnSubscribe_thenUnSubscribed() {
        String userName = "user1";
        String productId = "product1";
        User user = userService.getUserByName(userName);
        subscriptionService.unSubscribe(userName, productId);
        assertFalse(subscriptionService.getSubscriptionByProduct(productId).getUsers().contains(user));
        assertEquals(2, subscriptionService.getSubscriptions().size());
    }

    @Test
    void whenUnSubscribeNotSubscribedUser_thenExceptionThrown() {
        String userName = "user-new";
        userService.addUser(userName, false, 42);
        String productId = "product1";
        assertThrows(SubscriptionException.class, () -> subscriptionService.unSubscribe(userName, productId));
        assertEquals(2, subscriptionService.getSubscriptions().size());
    }

    @Test
    void whenNotifyUsers_thenUsersNotified() {
        String productId = "product1";
        subscriptionService.notifyUsers(productId);
        verify(spyDummyNotifier, times(1)).notifyUsers(anyList(), eq(productId));
    }

    @Test
    void whenGetExistingSubscriptionByProduct_thenReturnSubscription() {
        String productId = "product1";
        ProductSubscription subscription = subscriptionService.getSubscriptionByProduct(productId);
        assertNotNull(subscription);
    }

    @Test
    void whenGetNonExistingSubscriptionByProduct_thenReturnNull() {
        String productId = "product-new";
        productService.addNewProduct(productId, ProductCategory.BOOKS, false);

        ProductSubscription subscription = subscriptionService.getSubscriptionByProduct(productId);
        assertNull(subscription);
    }

    @Test
    void whenSortNonMedical_thenReturnInCorrectOrder() {
        List<User> users = subscriptionService.getSubscriptionByProduct("product1").getUsers();
        List<User> sortedUsers = subscriptionService.sortUsersByPriority(users, ProductCategory.DIGITAL);
        List<String> sortedUserNames = sortedUsers.stream().map(User::getName).collect(Collectors.toList());
        List<String> expectedUserNames = List.of("user2", "user6", "user1", "user3", "user4", "user5");
        assertEquals(expectedUserNames, sortedUserNames);
    }

    @Test
    void whenSortMedical_thenReturnInCorrectOrder() {
        List<User> users = subscriptionService.getSubscriptionByProduct("product2").getUsers();
        List<User> sortedUsers = subscriptionService.sortUsersByPriority(users, ProductCategory.MEDICAL);
        List<String> sortedUserNames = sortedUsers.stream().map(User::getName).collect(Collectors.toList());
        List<String> expectedUserNames = List.of("user2", "user4", "user6", "user1", "user3", "user5");
        assertEquals(expectedUserNames, sortedUserNames);
    }

    @Test
    void whenGetSubscriptions_thenSubscriptionReturned() {
        List<ProductSubscription> subscriptions = subscriptionService.getSubscriptions();
        assertEquals(2, subscriptions.size());
    }
}