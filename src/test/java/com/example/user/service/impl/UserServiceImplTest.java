package com.example.user.service.impl;

import com.example.exception.UserException;
import com.example.user.model.User;
import com.example.user.service.UserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceImplTest {
    UserService userService = UserServiceImpl.getInstance();

    @BeforeEach
    void initService() {
        userService.addUser("user1", false, 42);
        userService.addUser("user2", true, 32);
        userService.addUser("user3", false, 32);
    }

    @AfterEach
    void cleanUp() {
        userService.deleteAllUsers();
    }

    @Test
    void whenAddNewUser_thenUserAdded() {
        User user = userService.addUser("user4", false, 24);
        assertEquals(4, userService.getUsers().size());
        assertTrue(userService.getUsers().contains(user));
    }

    @Test
    void whenAddExistingUser_thenExceptionThrown() {
        assertThrows(UserException.class, () -> userService.addUser("user2", false, 24));
        assertEquals(3, userService.getUsers().size());
    }

    @Test
    void whenDeleteExistingUser_thenUserDeleted() {
        User user = userService.deleteUserByName("user2");
        assertEquals(2, userService.getUsers().size());
        assertFalse(userService.getUsers().contains(user));
    }

    @Test
    void whenDeleteNonExistingUser_thenUserDeleted() {
        User user = userService.deleteUserByName("user2");
        assertEquals(2, userService.getUsers().size());
        assertFalse(userService.getUsers().contains(user));
    }

    @Test
    void whenGetUsers_thenUsersReturned() {
        List<User> users = userService.getUsers();
        assertEquals(3, userService.getUsers().size());
    }

    @Test
    void whenGetUserByExistingName_thenReturnUser() {
        assertNotNull(userService.getUserByName("user1"));
    }

    @Test
    void whenGetUserByNonExistingName_thenExceptionThrown() {
        assertThrows(UserException.class,() -> userService.getUserByName("user5"));
    }

    @Test
    void whenDeleteAllUsers_thenUsersDeleted() {
        userService.deleteAllUsers();
        assertEquals(0, userService.getUsers().size());
    }

    @Test
    void whenModifyUserListDirectly_thenActualUserListNotChanged() {
        assertThrows(UnsupportedOperationException.class, () -> userService.getUsers().remove(1));
        assertEquals(3, userService.getUsers().size());

        assertThrows(UnsupportedOperationException.class, () -> userService.getUsers().clear());
        assertEquals(3, userService.getUsers().size());
    }
}